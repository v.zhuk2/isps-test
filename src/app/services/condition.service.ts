import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {GetConditionsResponseDto} from "../utils/conditions.utils";

@Injectable({
  providedIn: 'root',
})
export class ConditionService {
  private apiUrl = 'https://global.lakmus.org/Dictionaries/icpc2?IsPublic=true'

  constructor(private http: HttpClient) {
  }

  public getConditions(request: string): Observable<GetConditionsResponseDto[]> {
    return this.http.get<GetConditionsResponseDto[]>(this.apiUrl + '&search=' + request);
  }
}
