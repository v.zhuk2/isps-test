import {Component, OnInit} from '@angular/core';
import {debounceTime, distinctUntilChanged, Observable, switchMap, tap, using} from "rxjs";
import {FormControl, Validators} from "@angular/forms";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {ConditionService} from "./services/condition.service";
import {Code, Condition, Context, GetConditionsResponseDto, Visit} from "./utils/conditions.utils";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public conditions: Array<Condition> = [];
  public dateControl = new FormControl(new Date(), Validators.required);
  public today = new Date()
  public generatedJson: string = '';
  constructor(private readonly conditionService: ConditionService) {}

  performSearch(value: string): Observable<GetConditionsResponseDto[]> {
    return this.conditionService.getConditions(value);
  }

  addEmptyCondition() {
    let context: Context = {
      identifier: {
        type: {
          coding: [{code: 'encounter', system: 'eHealth/resources'}]
        },
        value: null
      }
    }
    let code: Code = {
      coding: [{code: null, system: 'eHealth/ICPC2/condition_codes'}]
    }
    let newCondition: Condition = {
      id: crypto.randomUUID(),
      code,
      context,
      notes: null,
      onsetDate: null,
      control: new FormControl(null, Validators.required),
      commentControl: new FormControl(''),
      searchResults: []
    }

    newCondition.control.setErrors({'incorrect': true});
    newCondition.control.valueChanges.pipe(
      tap(_ => newCondition.control.setErrors({'incorrect': true})),
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(value => this.performSearch(value))
    ).subscribe(x => newCondition.searchResults = x);

    this.conditions.push(newCondition)
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent, condition: Condition) {
    const value = event.option.value as GetConditionsResponseDto
    condition.code.coding[0].code = value.code;
    condition.context.identifier.value = value.id.toString();
    condition.onsetDate = new Date();
    condition.control.setErrors(null);
    console.log(event)
  }
  displayFn(result?: GetConditionsResponseDto): string | undefined {
    return result ? result.code + ' - ' + result.name : undefined;
  }

  deleteCondition(conditionId: string) {
    this.conditions = this.conditions.filter(x => x.id !== conditionId);
  }

  submitResults() {
    console.log('results submitted')
  }

  isValid(): boolean {
    let valid = true;
    if (this.conditions.length == 0) return false
    this.conditions.forEach(x => {
      if (!x.control.valid) valid = false
    })
    return valid;
  }

  generateJson(): void {
    let result: Visit = {
      encounter: {
        date: this.dateControl.value
      }
    }

    let conditions: Condition[] = this.conditions.map(x => {
      return {
        id: x.id,
        context: x.context,
        code: x.code,
        notes: x.commentControl.value,
        onsetDate: x.onsetDate,
      } as Condition
    })
    if (conditions.length != 0) {
        result = {...result, conditions: conditions}
    }
    this.generatedJson = JSON.stringify(result, null, 2);
  }
}
