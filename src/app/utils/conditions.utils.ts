import {FormControl} from "@angular/forms";

export interface Code {
  coding: Coding[];
}

export interface Coding {
  system: string;
  code: string;
}

export interface Condition {
  id: string;
  context: Context;
  code: Code;
  notes: string;
  onsetDate: Date;
  control: FormControl;
  commentControl: FormControl;
  searchResults: GetConditionsResponseDto[];
}

export interface Context {
  identifier: Identifier;
}

export interface Encounter {
  date: Date;
}

export interface Identifier {
  type: Type;
  value: string;
}

export interface Visit {
  encounter: Encounter;
  conditions?: Condition[];
}

export interface Type {
  coding: Coding[];
}

export interface GetConditionsResponseDto {
  id: number,
  code: string,
  name: string,
  "isPublic": true
}
